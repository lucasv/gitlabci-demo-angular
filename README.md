# Démonstration d'intégration continue avec GitLab sur un projet Angular

Le projet d'exemple est un template gratuit basé sur Angular 2 et Angular CLI:
https://www.creative-tim.com/product/material-dashboard-angular2

![Illustration](https://s3.amazonaws.com/creativetim_bucket/products/53/original/opt_md_angular_thumbnail.jpg?1510145199)

Sa documentation explique comment le générer :

1. Install NodeJs from [NodeJs Official Page](https://nodejs.org/en).
2. Open Terminal
3. Go to your file project
4. Run in terminal: ```npm install -g @angular/cli```
5. Then: ```npm install```
6. And: ```ng serve```
7. Navigate to `http://localhost:4200/`
